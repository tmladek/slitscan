"use strict";
const package_json = require("../package.json")

module.exports = {
  NODE_ENV: "\"production\"",
  VERSION: JSON.stringify(package_json.version),
  HOMEPAGE_URL: JSON.stringify(package_json.homepage)
};
