export function getDimsFit (wOriginal, hOriginal, wContain, hContain) {
  let origRatio = wOriginal / hOriginal;
  let containRatio = wContain / hContain;

  if (origRatio > containRatio) {
    return {
      width: wContain,
      height: wContain / origRatio,
    };
  } else {
    return {
      width: origRatio * hContain,
      height: hContain,
    };
  }
}
